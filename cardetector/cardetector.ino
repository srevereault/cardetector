/*
 * 
  Car Detector 
  By Sylvain REVEREAULT
  
  A simple distance detector to put on a bike

  Code based on : 
   - Ultrasonic Sensor HC-SR04 and Arduino Tutorial by Dejan Nedelkovski - www.HowToMechatronics.com
   - NeoPixel Userguide Arduino by MCHobby - wiki.mchobby.be/index.php?title=NeoPixel-UserGuide-Arduino

*/
// defines ultrasound sensor pins numbers
const int trigPin = 9;
const int echoPin = 10;

// defines variables for ultrasound sensor
long duration;
int distance;

// Led Strip initialisation and colors definitions
#include <Adafruit_NeoPixel.h>
#define PIN 5
Adafruit_NeoPixel strip = Adafruit_NeoPixel(30, PIN, NEO_GRB + NEO_KHZ800);
uint32_t redish = strip.Color(255, 20, 20);
uint32_t yellow = strip.Color(255, 255, 20);
uint32_t greeny = strip.Color(20, 255, 20);
uint32_t blacky = strip.Color(0, 0, 0);

void setup() {
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
  Serial.begin(9600); // Starts the serial communication

  strip.begin();
  strip.show(); // All pixels are "off"
}

void loop() {
  strip.show();
  // Clears the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);
  // Calculating the distance
  distance = duration * 0.034 / 2;
  // Prints the distance on the Serial Monitor
  Serial.print("Distance: ");
  Serial.println(distance);
  // If distance is too low, make the led strip blink red
  if (distance <= 20) {
    strip.fill(redish);
    strip.show();
    delay(200);
    strip.fill(blacky);
    strip.show();
    delay(200);
  }
  // If the distance is medium, the led strip shows yellow leds
  else if (distance <= 80) {
    strip.fill(yellow);
    strip.show();
  }
  // Else, the led strips are green
  else {
    strip.fill(greeny);
  }
}
