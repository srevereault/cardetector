# Simple car detector

## Used components
 - Arduino Card (an Arduino UNO is used for the prototype)
 - RGB led strip (for instance a WS2812 RGB LED strip)
 - Ultrasound sensor (a HC-SR04 is used for the prototype)

## Wiring

![CarDetector](cardetector_bb.png)